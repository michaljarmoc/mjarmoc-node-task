import { Controller, Get, Param, Inject, UnprocessableEntityException, BadRequestException } from '@nestjs/common';
import { IBestBuyer, IBestSeller } from '../Model/IReports';
import { OrderMapper } from '../../Order/Service/OrderMapper';

@Controller()
export class ReportController {

  @Inject() orderMapper: OrderMapper;

  /* 
  This endpoint has to return array of IBestSeller instead of single entity, 
  because their could be multiple products with the same amount sold on a particular day 
  */

  @Get('/report/products/:date')
  public async bestSellers(@Param('date') date: string): Promise<IBestSeller[]> {
    return this.orderMapper.getBestSellersByDay(date);
  }

  /* 
  This endpoint has to return array of IBestBuyer instead of single entity, 
  because their could be multiple customers with the same money spent on a particular day 
  */

  @Get('/report/customer/:date')
  public bestBuyers(@Param('date') date: string): Promise<IBestBuyer[]> {
    return this.orderMapper.getBestBuyers(date);
  }
}
