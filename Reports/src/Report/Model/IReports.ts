/**
 * bestseller products
 */
export interface IBestSeller {
  productName: string;
  quantity: number;
  totalPrice: number;
}

/**
 * Customer best buyers
 */
export interface IBestBuyer {
  customerName: string; // customer full name
  totalPrice: number; // total amount spent on all products
}