import { Injectable, Inject, UnprocessableEntityException, InternalServerErrorException } from '@nestjs/common';
import { Repository } from './Repository';
import { IBestSeller, IBestBuyer } from 'src/Report/Model/IReports';
import { Order } from '../Model/Order';
import { Product } from '../Model/Product';
import { Customer } from '../Model/Customer';

export interface AggregatedProduct {
  id: number;
  sold: number;
}

@Injectable()
export class OrderMapper {
  @Inject() repository: Repository;

  public async getBestSellersByDay(date: string): Promise<IBestSeller[]> {
    
    const ordersForDate: Order[] = await this.getOrdersForDate(date);
    let products: Product[];
    
    try {
      products = await this.repository.fetchProducts();
    } catch {
      throw new InternalServerErrorException('Can not load data from repository');
    }

    const bestSellers = ordersForDate

    /* Map Order To ProductsId Array */
    .map<Order['products']>((order: Order) => order.products)

    /* Flatten ProductsId Array */
    .reduce((acc: Order['products'], val: Order['products']) => acc.concat(val))

    /* Reduce over Aggregated Times Sold */
    .reduce<AggregatedProduct[]>((uniques: AggregatedProduct[], productId: number, index: number, array: Order['products']) => {

      /* Remove Duplications & Aggregate Once*/
      if (uniques.find(product => product.id === productId)) {
        return uniques;
      }
      const mapped = {id: productId, sold: array.filter(v => v === productId).length};
      return uniques.concat(mapped);
    }, [])

    /* Map to BestSeller Model */
    .map<IBestSeller>((bestSeller: AggregatedProduct) => {
      const product: Product = products.find(product => product.id === bestSeller.id);
      if (!product) {
        throw new UnprocessableEntityException('Can not find product data based on order data. Please check database consistency');
      }
      return {
        productName: product.name,
        quantity: bestSeller.sold,
        totalPrice: bestSeller.sold * product.price,
      }
    })

    /* Sort in Respect to totalPrice Desc */
    return this.showBestResults(bestSellers, 1, 'quantity');
  }

  public async getBestBuyers(date: string): Promise<IBestBuyer[]> {
    
    const ordersForDate: Order[] = await this.getOrdersForDate(date);

    let products: Product[];
    let customers: Customer[];
    
    try {
      customers = await this.repository.fetchCustomers();
      products = await this.repository.fetchProducts();
    } catch {
      throw new InternalServerErrorException('Can not load data from repository');
    }
    
    const bestBuyers = ordersForDate

    /* Aggregate Customer */
    .reduce<Order[]>((uniques: Order[], order: Order, index: number, array: Order[]) => {
      if (uniques.length === 0) {
        return [order];
      }
      const customerIndex = uniques.findIndex(v => v.customer === order.customer);
      if (customerIndex === -1) {
        return [
          ...uniques,
          order
        ];
      }
      return [
        ...uniques.slice(0, customerIndex),
        {
          ... uniques[customerIndex],
          products: [
            ... uniques[customerIndex].products,
            ... order.products
          ]
        },
        ...uniques.slice(customerIndex + 1)
      ];
    }, [])

    /* Map to Customer & Total */
    .map<IBestBuyer>((order: Order) => {
      const customer = customers.find((customer: Customer) => customer.id === order.customer);
      if (!customer) {
        throw new UnprocessableEntityException('Can not find customer data based on order data. Please check database consistency');
      }
      return {
      customerName: `${customer.firstName} ${customer.lastName}`,
      totalPrice: order.products.reduce((acc, productId) => {
        const product = products.find((product: Product) => product.id === productId);
        if (!product) {
          throw new UnprocessableEntityException('Can not find product data based on order data. Please check database consistency');
        }
        return acc + product.price;
      }, 0),
    }})
    
    return this.showBestResults(bestBuyers, 1, 'totalPrice');
  }
  
  /* Show Only the Best N Results */
  private showBestResults<T>(array: T[], resultsNumber: number, property: string): T[] {

    array = array.sort((a: T, b: T) => -(a[property] - b[property]));
    const values = new Set(array.map(v => v[property]));
    const returnValue = Array.from(values)[resultsNumber - 1];
    return array.filter(item => item[property] >= returnValue);
  }

  private async getOrdersForDate(date: string): Promise<Order[]>{
    
    let orders: Order[];
    
    try {
      orders = await this.repository.fetchOrders();
    } catch {
      throw new InternalServerErrorException('Can not load data from repository'); 
    }

    /* 
      Filter Orders by Date
      Remark: I believe this should be a part of Repository not DataService
    */
    const ordersForDate = orders.filter((order: Order) => order.createdAt === date);
    if (ordersForDate.length === 0) {
      throw new UnprocessableEntityException(`There is no order for ${date}`);
    }
    return ordersForDate;
  }
  

}
