import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ReportModule } from './../src/Report/ReportModule';
import { Repository } from './../src/Order/Service/Repository';

describe('Reports Endpoints', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [ReportModule],
    })
    /* In Real World We Would Use Current Repostiory to Stub Database One */
    .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  
  it('/report/customer/:date should return bestBuyer', () => {
    return request(app.getHttpServer())
      .get('/report/customer/2019-08-08')
      .expect([{
        customerName: 'Jane Doe',
        totalPrice: 110,
      }])
      .expect(200);
  });

  it('/report/customer/:date should return bestBuyer', () => {
    return request(app.getHttpServer())
      .get('/report/customer/2019-08-07')
      .expect([{
        customerName: 'John Doe',
        totalPrice: 135.75,
      }])
      .expect(200);
  });

  it('/report/customer/:date should throw error for no orders for the date', () => {
    return request(app.getHttpServer())
      .get('/report/customer/2001-08-01')
      .expect(422);
  });

  it('/report/customer/:date should throw error for wrong param', () => {
    return request(app.getHttpServer())
      .get('/report/customer/222')
      .expect(422);
  });

  it('/report/products/:date should return bestSeller', () => {
    return request(app.getHttpServer())
      .get('/report/products/2019-08-07')
      .expect([
        { 
        productName: 'Black sport shoes',
        quantity: 2,
        totalPrice: 220 
        }
      ])
      .expect(200);
  });
  
  it('/report/products/:date if multiple products has the highest sold quantity should return all of them)', () => {
    return request(app.getHttpServer())
      .get('/report/products/2019-08-08')
      .expect([
        { 
          productName: 'Black sport shoes',
          quantity: 1,
          totalPrice: 110 
        },
        {
          productName: 'Cotton t-shirt XL',
          quantity: 1,
          totalPrice: 25.75 
        },
        {
          productName: 'Blue jeans', 
          quantity: 1, 
          totalPrice: 55.99 
        }
      ])
      .expect(200);
  });

  it('/report/products/:date should throw error for no orders for the date', () => {
    return request(app.getHttpServer())
      .get('/report/products/2001-08-01')
      .expect(422);
  });

  it('/report/products/:date should throw error for wrong param', () => {
    return request(app.getHttpServer())
      .get('/report/products/222')
      .expect(422);
  });
  

});
