Hello :)

- Tests for Report: yarn test:e2e
- I have changed the reponse interfaces from IBestBuyer and IBestSeller to IBestBuyer[] and IBestSeller[] respectievly, since multiple items may have the same highest quantity of sales or buys.
- I was thinking about data param validation for the format, but using RegExp in public controllers method leads to security issue - thread overload (you can easily block thread for a significant time and resources with long string to check against regexp).
- I have added missing interfaces for data.
- I believe finding data that matches certain conditions like "find all orders createdAt the date" is a part of Repository pattern, but for the sake of task I have created that method in OrderMapper service.
- Repository does not handle exceptions, so I did it in OrderMapper, but again, it should be part of the Repository in the real world.
- I did quite a lot of comments, probably not all are necessary, but this is recruitment task, so I wanted to show my thought process.
- In real world scenario I would also use more abstraction and higher-level methods for data manipulation from Lodash, to make code cleaner. But again, since this is the recruitment task, I tried not to use additional 3-rd parties.

Looking forwards to feedback :)