/* BSort is a Quick Sort Algorithm */

export class BSort {
  
  public static sort<T>(arr: T[]): T[] {
    if (arr.length < 2) {
      return arr;
    }

    const currentIndex: number = arr.length - 1;
    const currentElement: T = arr[currentIndex];
    const a = [];
    const b = [];

    for (let i = 0; i < currentIndex; i++) {
      const temp = arr[i];
      temp < currentElement ? a.push(temp) : b.push(temp);
    }
  
    return [
      ...BSort.sort(a), 
      currentElement, 
      ...BSort.sort(b),
    ];

  }

}