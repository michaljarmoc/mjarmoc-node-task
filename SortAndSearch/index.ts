import { ASort } from './ASort';
import { BSort } from './BSort';
import { BSearch } from './BSearch';

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [1, 5, 13, 27, 77];

const sortedA: number[] = ASort.sort(unsorted);
const sortedB: number[] = BSort.sort(unsorted);
const Search = new BSearch();

/* 
  With Asset Node.js Imported if the project is inited
  assert.deepEqual(sortedA, sortedB);
*/
console.log(sortedA, sortedB);

elementsToFind.forEach(element => {
  console.log(`For element ${element} the search result is ${Search.elementInList(element, sortedA)}`);
});

console.log(`Total number of operation is ${Search.operations}`);