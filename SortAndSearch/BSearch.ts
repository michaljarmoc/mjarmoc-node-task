/* BSearch is a Binary Sort Algorithm */

export class BSearch {
  
  private _operations: number = 0;

  public get operations(): number {
    return this._operations;
  }

  public elementInList(element: number, list: number[]): number {
  
  this._operations++;
 
  /* Init slice boundries */
  let start: number = 0;
  let end: number = list.length - 1;
  let middle: number = Math.floor((start + end) / 2);

  /* Compare the element with the middle element in the list unless it is equal or there are no elements in remaining slice */
  while (list[middle] !== element && start < end) {
    
    /* If it is smaller than middle then remove the second half slice */
    if (element < list[middle]) {
      end = middle - 1
    } 
    /* If it is greater than middle then remove the first half slice */
    else {
      start = middle + 1
    }

    /* Calculate middle point of remaining half */
    middle = Math.floor((start + end) / 2);
  }
  
  /* Return Element or -1 if it was not found */
  return (list[middle] !== element) ? -1 : middle

  }
}