/* ASort is a Merge Sort Algorithm */

export class ASort {
  
  /* Split the array into halves and merge them recursively */
  public static sort<T>(arr: T[]): T[] {
    
    /* Guard for Single Element Array */
    if (arr.length === 1) {
      return arr;
    }
  
  /* Slice Array */
  const middle: number = Math.floor(arr.length / 2);
  const left: T[] = arr.slice(0, middle);
  const right: T[] = arr.slice(middle);
  
  /* Recursively Call */
  return ASort.merge(
    ASort.sort(left),
    ASort.sort(right)
  )
}

  /* Compare and Merge */
  private static merge<T>(left: T[], right: T[]): T[] {
    
    let result: T[] = [];
    let indexLeft: number = 0;
    let indexRight: number = 0;

    while (indexLeft < left.length && indexRight < right.length) {
      if (left[indexLeft] < right[indexRight]) {
        result.push(left[indexLeft]);
        indexLeft++;
      } else {
        result.push(right[indexRight]);
        indexRight++;
      }
    }
    return result.concat(left.slice(indexLeft)).concat(right.slice(indexRight));
  }

}